public class Student {
    // Declare Variable
    public String nama, email, NoHP;

    // Declare the Set Method
    public void setNama(String newNama) {
        this.nama = newNama;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public void setNoHP(String newNoHP) {
        this.NoHP = newNoHP;
    }

    // Declare the Get Method
    public String getNama() {
        return this.nama;
    }

    public String getEmail() {
        return this.email;
    }

    public String getNoHP() {
        return this.NoHP;
    }
}
