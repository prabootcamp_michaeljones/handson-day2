
//Importing Scanner function
import java.util.Scanner;

public class MyApp extends Student {
    public static void main(String[] args) {
        // Setup Scanner and Student
        Scanner keyboard = new Scanner(System.in);
        Student student = new Student();

        // Input Student Data
        System.out.print("Ketik Nama, Email & no. HP: ");
        String input = keyboard.nextLine();
        String[] data = input.split(" ");
        System.out.println("Student: " + student);

        // Set and Get the Student data
        student.setNama(data[0]);
        student.setEmail(data[1]);
        student.setNoHP(data[2]);
        System.out.println("Student.getName(): " + student.getNama());
        System.out.println("Student.getEmail(): " + student.getEmail());
        System.out.println("Student.getPhoneNumber(): " + student.getNoHP());
    }
}
